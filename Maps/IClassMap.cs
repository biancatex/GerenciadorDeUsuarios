using Microsoft.EntityFrameworkCore;

namespace GerenciadoDeUsuarios.Maps
{
    public interface IClassMap
    {
         void Mapear(ModelBuilder modelBuilder);
    }
}